package com.enkosi.lendingApplicaption.web_rest;

import com.enkosi.lendingApplicaption.service.BookService;
import com.enkosi.lendingApplicaption.service.dto.BookDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * REST controller for managing {@link com.enkosi.lendingApplicaption.domain.Book}.
 */
@RestController
@RequestMapping("/api")
public class BookRessource {

    private final BookService bookService;

    private static final String ENTITY_NAME = "book";

    private ArrayList<BookDTO> list;

    public BookRessource(BookService bookService) {
        this.bookService = bookService;
    }

    /**
     * {@code POST  /books} : Create a new book.
     *
     * @param bookDTO the bookDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bookDTO, or with status {@code 400 (Bad Request)} if the book has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/books")
    public ResponseEntity<String> createBook(@RequestBody BookDTO bookDTO) throws Exception {
        if (bookDTO.getId() != null) {
            return ResponseEntity.badRequest()
                    .body("A new book cannot have an ID");
        }
        BookDTO result = bookService.save(bookDTO);
        return ResponseEntity.created(new URI("/api/books/" + result.getId()))
                .header("Creation of the book entity " + result.getTitle(), result.getId().toString())
                .body(result.toString());
    }


    /**
     * {@code PUT  /books} : Updates an existing book.
     *
     * @param bookDTO the bookDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bookDTO,
     * or with status {@code 400 (Bad Request)} if the bookDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bookDTO couldn't be updated.
     */
    @PutMapping("/books")
    public ResponseEntity<String> updateContrat(@RequestBody BookDTO bookDTO) throws Exception {
        if (bookDTO.getId() == null) {
            return ResponseEntity.badRequest()
                    .body("A book must have an ID in order to be update");
        }
        BookDTO result = bookService.save(bookDTO);
        return ResponseEntity.ok()
                .header("Update of the book entity : title " + result.getTitle() + " and ID ", result.getId().toString())
                .body(result.toString());
    }

    /**
     * {@code GET  /books} : get all the books.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of books in body.
     */
    @GetMapping("/books")
    public ResponseEntity<ArrayList<BookDTO>> getAllContrats(@RequestParam MultiValueMap<String, String> queryParams) {
        list = bookService.findAll();
        return ResponseEntity.ok()
                .header("Here is the complete list of books downloaded from the database ", String.valueOf(list))
                .body(list);
    }

    /**
     * {@code GET  /books/:id} : get the "id" book.
     *
     * @param id the id of the bookDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bookTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contrats/{id}")
    public Optional<BookDTO> getBook(@PathVariable Integer id) {
        return  bookService.findOne(id);
    }

}
