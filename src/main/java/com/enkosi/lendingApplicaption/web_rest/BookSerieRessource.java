package com.enkosi.lendingApplicaption.web_rest;


import com.enkosi.lendingApplicaption.service.BookSerieService;
import com.enkosi.lendingApplicaption.service.dto.BookSerieDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * REST controller for managing {@link com.enkosi.lendingApplicaption.domain.BookSerie}.
 */
@RestController
@RequestMapping("/api")
public class BookSerieRessource {


    private BookSerieService bookSerieService;

    private ArrayList<BookSerieDTO> list;

    public BookSerieRessource(BookSerieService bookSerieService) {
        this.bookSerieService = bookSerieService;
    }

    /**
     * {@code POST  /bookSeries} : Create a new bookSerie.
     *
     * @param bookSerieDTO the bookSerieDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bookSerieDTO, or with status {@code 400 (Bad Request)} if the bookSerie has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bookSeries")
    public ResponseEntity<String> createBookSerie(@RequestBody BookSerieDTO bookSerieDTO) throws Exception {
        if (bookSerieDTO.getId() != null) {
            return ResponseEntity.badRequest()
                    .body("A new bookSerie cannot have an ID");
        }
        BookSerieDTO result = bookSerieService.save(bookSerieDTO);
        return ResponseEntity.created(new URI("/api/bookSeries/" + result.getId()))
                .header("Creation of the bookSerie entity " + result.getName(), result.getId().toString())
                .body(result.toString());
    }


    /**
     * {@code PUT  /bookSeries} : Updates an existing bookSerie.
     *
     * @param bookSerieDTO the bookSerieDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bookSerieDTO,
     * or with status {@code 400 (Bad Request)} if the bookSerieDTO is not valid,
     */
    // TODO or with status {@code 500 (Internal Server Error)} if the bookSerieDTO couldn't be updated.
    @PutMapping("/bookSeries")
    public ResponseEntity<String> updateBookSerie(@RequestBody BookSerieDTO bookSerieDTO) throws Exception {
        if (bookSerieDTO.getId() == null) {
            return ResponseEntity.badRequest()
                    .body("A bookSerie must have an ID in order to be update");
        }
        BookSerieDTO result = bookSerieService.save(bookSerieDTO);
        return ResponseEntity.ok()
                .header("Update of the bookSerie entity : " + result.getName() + " ID ", result.getId().toString())
                .body(result.toString());
    }

    /**
     * {@code GET  /bookSeries} : get all the bookSeries.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookSeries in body.
     */
    @GetMapping("/bookSeries")
    public ResponseEntity<ArrayList<BookSerieDTO>> getAllBookSeries() throws Exception {
        list = bookSerieService.findAll();
        return ResponseEntity.ok()
                .header("Here is the complete list of bookSeries downloaded from the database ", String.valueOf(list))
                .body(list);
    }

    /**
     * {@code GET  /bookSeries/:id} : get the "id" bookSerie.
     *
     * @param id the id of the bookSerieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bookSerieTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bookSeries/{id}")
    public Optional<BookSerieDTO> getBookSerie(@PathVariable Integer id) {
        return bookSerieService.findOne(id);
    }

}
