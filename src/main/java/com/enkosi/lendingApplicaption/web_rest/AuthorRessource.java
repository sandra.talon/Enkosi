package com.enkosi.lendingApplicaption.web_rest;


import com.enkosi.lendingApplicaption.service.AuthorService;
import com.enkosi.lendingApplicaption.service.dto.AuthorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * REST controller for managing {@link com.enkosi.lendingApplicaption.domain.Author}.
 */
@RestController
@RequestMapping("/api")
public class AuthorRessource {

    private AuthorService authorService;

    private ArrayList<AuthorDTO> list;

    public AuthorRessource(AuthorService authorService) {
        this.authorService = authorService;
    }

    /**
     * {@code POST  /authors} : Create a new author.
     *
     * @param authorDTO the authorDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new authorDTO, or with status {@code 400 (Bad Request)} if the author has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/authors")
    public ResponseEntity<String> createAuthor(@RequestBody AuthorDTO authorDTO) throws Exception {
        if (authorDTO.getId() != null) {
            return ResponseEntity.badRequest()
                    .body("A new author cannot have an ID");
        }
        AuthorDTO result = authorService.save(authorDTO);
        return ResponseEntity.created(new URI("/api/authors/" + result.getId()))
                .header("Creation of the author entity " + result.getName() + " " + result.getSurname(), result.getId().toString())
                .body(result.toString());
    }


    /**
     * {@code PUT  /authors} : Updates an existing author.
     *
     * @param authorDTO the authorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated authorDTO,
     * or with status {@code 400 (Bad Request)} if the authorDTO is not valid,
     */
    // TODO or with status {@code 500 (Internal Server Error)} if the authorDTO couldn't be updated.
    @PutMapping("/authors")
    public ResponseEntity<String> updateAuthor(@RequestBody AuthorDTO authorDTO) throws Exception {
        if (authorDTO.getId() == null) {
            return ResponseEntity.badRequest()
                    .body("A author must have an ID in order to be update");
        }
        AuthorDTO result = authorService.save(authorDTO);
        return ResponseEntity.ok()
                .header("Update of the author entity : " + result.getName() + " " + result.getSurname() + " ID ", result.getId().toString())
                .body(result.toString());
    }

    /**
     * {@code GET  /authors} : get all the authors.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of authors in body.
     */
    @GetMapping("/authors")
    public ResponseEntity<ArrayList<AuthorDTO>> getAllAuthors() throws Exception {
        list = authorService.findAll();
        return ResponseEntity.ok()
                .header("Here is the complete list of authors downloaded from the database ", String.valueOf(list))
                .body(list);
    }

    /**
     * {@code GET  /authors/:id} : get the "id" author.
     *
     * @param id the id of the authorDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the authorTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/authors/{id}")
    public Optional<AuthorDTO> getAuthor(@PathVariable Integer id) {
        return authorService.findOne(id);
    }

}
