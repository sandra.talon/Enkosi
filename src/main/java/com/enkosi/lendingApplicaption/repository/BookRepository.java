package com.enkosi.lendingApplicaption.repository;

import com.enkosi.lendingApplicaption.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



/**
 * Spring Data  repository for the Book entity.
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

    int countAllByAuthorAndTitle (Integer author, String Title);
    int countAllByAuthorAndTitleAndIdIsNot(Integer author, String Title, Integer id);
}
