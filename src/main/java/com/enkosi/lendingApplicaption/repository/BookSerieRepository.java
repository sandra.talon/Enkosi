package com.enkosi.lendingApplicaption.repository;


import com.enkosi.lendingApplicaption.domain.BookSerie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Book Serie entity.
 */
@Repository
public interface BookSerieRepository extends JpaRepository<BookSerie, Integer>  {

    int countAllByNameAndIdIsNot(String name, Integer id);
    int countAllByName(String name);
}
