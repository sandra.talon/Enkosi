package com.enkosi.lendingApplicaption.repository;

import com.enkosi.lendingApplicaption.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Author entity.
 */
@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {

    int countAllByNameAndSurname(String name, String surname);
    int countAllByNameAndSurnameAndIdIsNot(String name, String surname, Integer id);
}
