package com.enkosi.lendingApplicaption.domain;



import javax.persistence.*;

/**
 * A Book.
 */
@Entity
@Table(name ="book")
public class Book {

    @Id
    @Column(name= "idBook", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="author_idAuthor")
    private Author author;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="serie_idSerie")
    private BookSerie bookSerie;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public BookSerie getBookSerie() {
        return bookSerie;
    }

    public void setBookSerie(BookSerie bookSerie) {
        this.bookSerie = bookSerie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Book)) {
            return false;
        }
        return id != null && id.equals(((Book) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + getId() +
                ", title='" + getTitle() + "'" +
                ", authors='" + getAuthor() + "'" +
                ", book series='" + getBookSerie() + "'" +
                "}";
    }
}
