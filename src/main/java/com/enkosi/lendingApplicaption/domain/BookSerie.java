package com.enkosi.lendingApplicaption.domain;

import javax.persistence.*;

/**
 * A Book Serie.
 */
@Entity
@Table(name ="bookSerie")
public class BookSerie {

    @Id
    @Column(name= "iBookSerie", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    // TODO : add validators @NotNull
    private String name;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BookSerie)) {
            return false;
        }
        return id != null && id.equals(((BookSerie) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Book Serie{" +
                "id=" + getId() +
                ", name='" + getName() + "'" +
                "}";
    }
}
