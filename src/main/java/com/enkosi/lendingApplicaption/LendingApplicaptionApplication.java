package com.enkosi.lendingApplicaption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LendingApplicaptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(LendingApplicaptionApplication.class, args);
	}

}
