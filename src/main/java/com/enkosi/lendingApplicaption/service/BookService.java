package com.enkosi.lendingApplicaption.service;

import com.enkosi.lendingApplicaption.domain.Book;
import com.enkosi.lendingApplicaption.repository.BookRepository;
import com.enkosi.lendingApplicaption.service.dto.BookDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.enkosi.lendingApplicaption.service.mapper.BookMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BookService {

    private BookRepository bookRepository;

    private BookMapper bookMapper;

    private ArrayList<BookDTO> list;

    private static final String ENTITY_NAME = "book";

    public BookService(BookRepository bookRepository, BookMapper bookMapper) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
    }

    /**
     * Save a book.
     *
     * @param bookDTO the entity to save.
     * @return the persisted entity.
     */
    public BookDTO save(BookDTO bookDTO) throws Exception {
        if (bookDTO.getTitle() == null) {
            throw new Exception("A book must have a title");
        }
        if (bookDTO.getAuthor_idAuthor() == null) {
            throw new Exception("A book must have an author");
        }
        if (bookDTO.getId() == null) {
            if (bookRepository.countAllByAuthorAndTitle(bookDTO.getAuthor_idAuthor(), bookDTO.getTitle()) != 0) {
                throw new Exception("The book you are trying to save already exists in the Database");
            }
            else {
                if (bookRepository.countAllByAuthorAndTitleAndIdIsNot(bookDTO.getAuthor_idAuthor(), bookDTO.getTitle(), bookDTO.getId()) != 0) {
                    throw new Exception("The book you are trying to save already exists in the Database");
                }
            }
        }
        com.enkosi.lendingApplicaption.domain.Book savedBook = bookRepository.save(bookMapper.toEntity(bookDTO));
        return bookMapper.toDto(savedBook);
    }

    /**
     * find all the books.
     *
     * @return the list of all the books.
     */public ArrayList<BookDTO> findAll() {
        List<Book> bookList = bookRepository.findAll();
        for (Book book : bookList) {
            list.add(bookMapper.toDto(book));

        }
        return list;
    }


    /**
     * Get one book by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BookDTO> findOne(Integer id) {
        return bookRepository.findById(id)
                .map(bookMapper::toDto);
    }
}
