package com.enkosi.lendingApplicaption.service.dto;


public class BookDTO {

    private Integer id;

    private String title;

    private Integer author_idAuthor;

    private Integer bookSerie_idBookSerie;

    private String volume;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAuthor_idAuthor() {
        return author_idAuthor;
    }

    public void setAuthor_idAuthor(Integer author_idAuthor) {
        this.author_idAuthor = author_idAuthor;
    }

    public Integer getBookSerie_idBookSerie() {
        return bookSerie_idBookSerie;
    }

    public void setBookSerie_idBookSerie(Integer bookSerie_idBookSerie) {
        this.bookSerie_idBookSerie = bookSerie_idBookSerie;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + getId() +
                ", title='" + getTitle() + "'" +
                ", authors='" + getAuthor_idAuthor() + "'" +
                ", book series='" + getBookSerie_idBookSerie() + "'" +
                ", volume='" + getVolume() + "'" +
                "}";
    }
}
