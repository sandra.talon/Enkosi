package com.enkosi.lendingApplicaption.service;

import com.enkosi.lendingApplicaption.domain.BookSerie;
import com.enkosi.lendingApplicaption.repository.BookSerieRepository;
import com.enkosi.lendingApplicaption.service.dto.BookSerieDTO;
import com.enkosi.lendingApplicaption.service.mapper.BookSerieMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class BookSerieService {

    private BookSerieRepository bookSerieRepository;

    private BookSerieMapper bookSerieMapper;

    private ArrayList<BookSerieDTO> list = new ArrayList<>();

    public BookSerieService(BookSerieRepository bookSerieRepository, BookSerieMapper bookSerieMapper) {
        this.bookSerieRepository = bookSerieRepository;
        this.bookSerieMapper = bookSerieMapper;
    }


    /**
     * Save a bookSerie.
     *
     * @param bookSerieDTO the entity to save.
     * @return the persisted entity.
     */
    public BookSerieDTO save(BookSerieDTO bookSerieDTO) throws Exception {
        if (bookSerieDTO.getName() == null) {
            throw new Exception("An bookSerie must have a name");
        }
        if (bookSerieDTO.getId() == null) {
            if (bookSerieRepository.countAllByName(bookSerieDTO.getName()) != 0) {
                throw new Exception("The bookSerie you are trying to save already exists in the Database");
            }
        }
        // TODO: add EnkosiExceptions
        else {
            if (bookSerieRepository.countAllByNameAndIdIsNot(bookSerieDTO.getName(), bookSerieDTO.getId()) != 0) {
                throw new Exception("The id of a bookSerie cannot be updated");
            }
        }
        BookSerie savedBook = bookSerieRepository.save(bookSerieMapper.toEntity(bookSerieDTO));
        return bookSerieMapper.toDto(savedBook);
    }


    /**
     * find all the bookSeries.
     *
     * @return the list of all the bookSeries.
     */public ArrayList<BookSerieDTO> findAll() throws Exception {
        List<BookSerie> bookSerieList = bookSerieRepository.findAll();
        if (bookSerieList.isEmpty()){
            throw new Exception("There is no bookSerie in the Database");
        }
        for (BookSerie bookSerie : bookSerieList) {
            list.add(bookSerieMapper.toDto(bookSerie));

        }
        return list;
    }


    /**
     * Get one bookSerie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BookSerieDTO> findOne(Integer id) {
        return bookSerieRepository.findById(id)
                .map(bookSerieMapper::toDto);
    }
}
