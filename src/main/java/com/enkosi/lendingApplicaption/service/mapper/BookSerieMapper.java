package com.enkosi.lendingApplicaption.service.mapper;

import com.enkosi.lendingApplicaption.domain.BookSerie;
import com.enkosi.lendingApplicaption.service.dto.BookSerieDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link com.enkosi.lendingApplicaption.domain.BookSerie} and its DTO {@link com.enkosi.lendingApplicaption.service.dto.BookSerieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BookSerieMapper extends EntityMapper<BookSerieDTO, BookSerie>{

    default BookSerie fromId(Integer id) {
        if (id == null) {
            return null;
        }
        BookSerie bookSerie = new BookSerie();
        bookSerie.setId(id);
        return bookSerie;
    }
}
