package com.enkosi.lendingApplicaption.service.mapper;


import com.enkosi.lendingApplicaption.domain.Book;
import com.enkosi.lendingApplicaption.service.dto.BookDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link com.enkosi.lendingApplicaption.domain.Book} and its DTO {@link com.enkosi.lendingApplicaption.service.dto.BookDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BookMapper extends EntityMapper<BookDTO, Book>{

    @Mapping(source = "author.id", target = "author_idAuthor")
    @Mapping(source = "bookSerie.id", target = "bookSerie_idBookSerie")
    BookDTO toDto(Book book);

    @Mapping(source = "author_idAuthor", target = "author.id")
    @Mapping(source = "bookSerie_idBookSerie", target = "bookSerie.id")
    Book toEntity(BookDTO bookDTO);

    default Book fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Book book = new Book();
        book.setId(id);
        return book;
    }

}
