package com.enkosi.lendingApplicaption.service.mapper;

import com.enkosi.lendingApplicaption.domain.Author;
import com.enkosi.lendingApplicaption.service.dto.AuthorDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link com.enkosi.lendingApplicaption.domain.Author} and its DTO {@link com.enkosi.lendingApplicaption.service.dto.AuthorDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AuthorMapper extends EntityMapper<AuthorDTO, Author> {

    default Author fromId(Integer id) {
        if (id == null) {
            return null;
        }
        Author author = new Author();
        author.setId(id);
        return author;
    }

}
