package com.enkosi.lendingApplicaption.service;

import com.enkosi.lendingApplicaption.domain.Author;
import com.enkosi.lendingApplicaption.repository.AuthorRepository;
import com.enkosi.lendingApplicaption.service.dto.AuthorDTO;
import com.enkosi.lendingApplicaption.service.mapper.AuthorMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AuthorService {

    private AuthorRepository authorRepository;

    private AuthorMapper authorMapper;

    private ArrayList<AuthorDTO> list = new ArrayList<>();

    public AuthorService(AuthorRepository authorRepository, AuthorMapper authorMapper) {
        this.authorRepository = authorRepository;
        this.authorMapper = authorMapper;
    }


    /**
     * Save a author.
     *
     * @param authorDTO the entity to save.
     * @return the persisted entity.
     */
    public AuthorDTO save(AuthorDTO authorDTO) throws Exception {
        if (authorDTO.getName() == null) {
            throw new Exception("An author must have a name");
        }
        if (authorDTO.getSurname() == null) {
            throw new Exception("An author must have a surname");
        }
        if (authorDTO.getId() == null) {
            if (authorRepository.countAllByNameAndSurname(authorDTO.getName(), authorDTO.getSurname()) != 0) {
                throw new Exception("The author you are trying to save already exists in the Database");
            }
        }
        else {
            if (authorRepository.countAllByNameAndSurnameAndIdIsNot(authorDTO.getName(), authorDTO.getSurname(), authorDTO.getId()) != 0) {
                throw new Exception("The id of an author cannot be updated");
            }
        }
        Author savedBook = authorRepository.save(authorMapper.toEntity(authorDTO));
        return authorMapper.toDto(savedBook);
    }


    /**
     * find all the authors.
     *
     * @return the list of all the authors.
     */public ArrayList<AuthorDTO> findAll() throws Exception {
        List<Author> authorList = authorRepository.findAll();
        if (authorList.isEmpty()){
            throw new Exception("There is no author in the Database");
        }
        for (Author author : authorList) {
            list.add(authorMapper.toDto(author));

        }
        return list;
    }


    /**
     * Get one author by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AuthorDTO> findOne(Integer id) {
        return authorRepository.findById(id)
                .map(authorMapper::toDto);
    }
}
