package com.enkosi.lendingApplicaption.service;

import com.enkosi.lendingApplicaption.domain.BookSerie;
import com.enkosi.lendingApplicaption.repository.BookSerieRepository;
import com.enkosi.lendingApplicaption.repository.BookSerieRepositoryTestUtils;
import com.enkosi.lendingApplicaption.service.dto.BookSerieDTO;
import com.enkosi.lendingApplicaption.service.mapper.BookSerieMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest()
public class BookSerieServiceIT {


    @Autowired
    private BookSerieMapper bookSerieMapper;

    @Autowired
    private BookSerieRepository bookSerieRepository;

    @Autowired
    private BookSerieRepositoryTestUtils bookSerieRepositoryTestUtils;

    @Autowired
    private BookSerieService bookSerieService;

    private BookSerie bookSerie;
    private BookSerie bookSerie2;

    private static String DEFAULT_NAME = "AAAAA";
    private static String UPDATED_NAME = "BBBBB";


    @BeforeEach
    @AfterEach
    @Transactional
    public void afterEach() {
        bookSerieRepositoryTestUtils.deleteAllByNameIsLike(DEFAULT_NAME);
        bookSerieRepositoryTestUtils.flush();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookSerie createEntity() {
        BookSerie bookSerie = new BookSerie();
        bookSerie.setName(DEFAULT_NAME);
        return bookSerie;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookSerie createUpdatedEntity() {
        BookSerie updatedBookSerie = new BookSerie();
        updatedBookSerie.setName(UPDATED_NAME);
        return updatedBookSerie;
    }


    /*
    ********SAVE
    * ********SHOULD WORK
     */
    @DisplayName("Save Non Existing BookSerie Test")
    @Test
    @Transactional
    public void testSaveNonExistingBookSerie() throws Exception {
        // Checking the size of the Databse before insertion
        int databaseSizeBeforeCreation = bookSerieRepository.findAll().size();

        // Saving the bookSerie
        bookSerie = createEntity();
        BookSerieDTO savedBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));

        // Checking the saved bookSerie
        assertNotNull(savedBookSerieDto);
        assertNotNull(savedBookSerieDto.getId());
        assertEquals(DEFAULT_NAME, savedBookSerieDto.getName());


        // Checking that the size of the Database after insertion
        int databaseSizeAfterCreation = bookSerieRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation+1, databaseSizeAfterCreation);
    }


    /*
     ********SAVE
     * ********SHOULD NOT WORK
     */
    @DisplayName("Save Non Existing BookSerie without Name Test")
    @Test
    @Transactional
    public void testSaveNonExistingBookSerieWithoutName()  {
        // Creation of the bookSerie without Name
        bookSerie = createEntity();
        bookSerie.setName(null);

        // Checking the Database size
        int databaseSizeBeforeCreation = bookSerieRepository.findAll().size();

        // The request must fail and send an exception
        try {
            BookSerieDTO failingBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));
            fail("Should throw exception when saving a new bookSerie without a name");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("An bookSerie must have a name"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = bookSerieRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }


    /*
     ********UPDATE
     * ********SHOULD WORK
     */
    @DisplayName("Update an bookSerie Test")
    @Test
    @Transactional
    public void testUpdateAnBookSerie() throws Exception {
        // Saving the bookSerie in the Database
        bookSerie = createEntity();
        BookSerieDTO savedBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));

        int databaseSizeBeforeUpdate= bookSerieRepository.findAll().size();

        // Updating the bookSerie
        BookSerie bookSerie2 = createUpdatedEntity();
        bookSerie2.setId(savedBookSerieDto.getId());
        BookSerieDTO savedBookSerie2Dto = bookSerieService.save(bookSerieMapper.toDto(bookSerie2));

        assertNotNull(savedBookSerie2Dto);
        assertEquals(savedBookSerieDto.getId(), savedBookSerie2Dto.getId());
        assertEquals(UPDATED_NAME, savedBookSerie2Dto.getName());

        // Checking that the size of the Database is still the same
        int databaseSizeAfterUpdate = bookSerieRepository.findAll().size();
        assertEquals(databaseSizeBeforeUpdate, databaseSizeAfterUpdate);
    }


    /*
     ********UPDATE
     * ********SHOULD NOT WORK
     */
    @DisplayName("Update the Id of an bookSerie Test")
    @Test
    @Transactional
    public void testUpdateIdExistingBookSerie() throws Exception {
        // Creation of the bookSerie
        bookSerie = createEntity();
        bookSerie.setId(1);

        // Saving the bookSerie in the Database
        BookSerieDTO savedBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));

        // Setting another ID
        bookSerie.setId((2));

        // Checking the Database size
        int databaseSizeBeforeCreation = bookSerieRepository.findAll().size();

        // The request must fail and send an exception
        try {
            bookSerieService.save(bookSerieMapper.toDto(bookSerie));
            fail("Should throw exception when saving a new bookSerie with an ID");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("The id of a bookSerie cannot be updated"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = bookSerieRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }

    @DisplayName("Update an BookSerie without Id Test")
    @Test
    @Transactional
    public void testUpdateBookSerieWithoutId() throws Exception {
        // Saving the bookSerie in the Database
        bookSerie = createEntity();
        BookSerieDTO savedBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));

        // Checking the Database size
        int databaseSizeBeforeCreation = bookSerieRepository.findAll().size();

        // The request must fail and send an exception
        try {
            bookSerieService.save(bookSerieMapper.toDto(bookSerie));
            fail("Should throw exception when saving an existing bookSerie without an Id");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("The bookSerie you are trying to save already exists in the Database"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = bookSerieRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }

    @DisplayName("Update an BookSerie without Name Test")
    @Test
    @Transactional
    public void testUpdateBookSerieWithoutName() throws Exception {
        // Saving the bookSerie in the Database
        bookSerie = createEntity();
        BookSerieDTO savedBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));

        // Checking the Database size
        int databaseSizeBeforeCreation = bookSerieRepository.findAll().size();

        // Setting a null name
        bookSerie.setName(null);
        bookSerie.setId(savedBookSerieDto.getId());

        // The request must fail and send an exception
        try {
            BookSerieDTO failingBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));
            fail("Should throw exception when updating an bookSerie without a name");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("An bookSerie must have a name"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = bookSerieRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }


    /*
     ********FIND ALL
     * ********SHOULD WORK
     */
    @DisplayName("Find all bookSeries Test")
    @Test
    @Transactional
    public void testFindAllBookSeries() throws Exception {
        bookSerie = createEntity();
        BookSerieDTO savedBookSerieDto = bookSerieService.save(bookSerieMapper.toDto(bookSerie));
        bookSerie2 = createUpdatedEntity();
        BookSerieDTO savedBookSerieDto2 = bookSerieService.save(bookSerieMapper.toDto(bookSerie2));


        int databaseSizeRep = bookSerieRepository.findAll().size();
        int databaseSizeService = bookSerieService.findAll().size();

        assertEquals(databaseSizeRep, databaseSizeService);
        assertNotNull(databaseSizeRep);
        assertNotNull(databaseSizeService);
    }

    /*
    ********FIND ALL
    * ********SHOULD NOT WORK
    */
    @DisplayName("Find all bookSeries - no bookSerie in Database Test")
    @Test
    @Transactional
    public void testFindAllBookSeriesNoBookSerieInDB() {
        // Checking that the Database is empty
        ArrayList<BookSerie> bookSerieList = (ArrayList<BookSerie>) bookSerieRepository.findAll();
        assertTrue(bookSerieList.isEmpty());

        // The request must fail and send an exception
        try {
            ArrayList<BookSerieDTO> failingBookSerieList = bookSerieService.findAll();
            fail("Should throw exception when there is no bookSerie in the Database");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("There is no bookSerie in the Database"));
        }
    }

    /*
     ********FIND ONE
     * ********SHOULD WORK
     */
    @DisplayName("Find an bookSerie with Id Test")
    @Test
    @Transactional
    public void testFindOneBookSeries() throws Exception {
        // Saving the bookSerie in the Database
        bookSerie = createEntity();
        BookSerieDTO savedBookSerieDTO = bookSerieService.save(bookSerieMapper.toDto(bookSerie));

        Optional<BookSerie> foundBookSerieRepo = bookSerieRepository.findById(savedBookSerieDTO.getId());
        Optional<BookSerieDTO> foundBookSerieService = bookSerieService.findOne(savedBookSerieDTO.getId());

        assertTrue(foundBookSerieRepo.isPresent());
        assertTrue(foundBookSerieService.isPresent());
        assertEquals(foundBookSerieRepo.get().getId(), foundBookSerieService.get().getId());
        assertEquals(foundBookSerieRepo.get().getName(), foundBookSerieService.get().getName());

    }
}
