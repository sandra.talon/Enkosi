package com.enkosi.lendingApplicaption.service;

import com.enkosi.lendingApplicaption.domain.Author;
import com.enkosi.lendingApplicaption.repository.AuthorRepository;
import com.enkosi.lendingApplicaption.repository.AuthorRepositoryTestUtils;
import com.enkosi.lendingApplicaption.service.dto.AuthorDTO;
import com.enkosi.lendingApplicaption.service.mapper.AuthorMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest()
public class AuthorServiceIT {


    @Autowired
    private AuthorMapper authorMapper;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorRepositoryTestUtils authorRepositoryTestUtils;

    @Autowired
    private AuthorService authorService;

    private Author author;
    private Author author2;

    private static String DEFAULT_NAME = "JULES";
    private static String DEFAULT_SURNAME = "DUPONT";

    private static String UPDATED_NAME = "MARC";
    private static String UPDATED_SURNAME = "DULAC";


    @BeforeEach
    @AfterEach
    @Transactional
    public void afterEach() {
        authorRepositoryTestUtils.deleteAllByNameIsLike(DEFAULT_NAME);
        authorRepositoryTestUtils.flush();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Author createEntity() {
        Author author = new Author();
        author.setName(DEFAULT_NAME);
        author.setSurname(DEFAULT_SURNAME);
        return author;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Author createUpdatedEntity() {
        Author updatedAuthor = new Author();
        updatedAuthor.setName(UPDATED_NAME);
        updatedAuthor.setSurname(UPDATED_SURNAME);
        return updatedAuthor;
    }


    /*
    ********SAVE
    * ********SHOULD WORK
     */
    @DisplayName("Save Non Existing Author Test")
    @Test
    @Transactional
    public void testSaveNonExistingAuthor() throws Exception {
        // Checking the size of the Databse before insertion
        int databaseSizeBeforeCreation = authorRepository.findAll().size();

        // Saving the author
        author = createEntity();
        AuthorDTO savedAuthorDto = authorService.save(authorMapper.toDto(author));

        // Checking the saved author
        assertNotNull(savedAuthorDto);
        assertNotNull(savedAuthorDto.getId());
        assertEquals(DEFAULT_NAME, savedAuthorDto.getName());
        assertEquals(DEFAULT_SURNAME, savedAuthorDto.getSurname());


        // Checking that the size of the Database after insertion
        int databaseSizeAfterCreation = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation+1, databaseSizeAfterCreation);
    }


    /*
     ********SAVE
     * ********SHOULD NOT WORK
     */
    @DisplayName("Save Non Existing Author without Name Test")
    @Test
    @Transactional
    public void testSaveNonExistingAuthorWithoutName()  {
        // Creation of the author without Name
        author = createEntity();
        author.setName(null);

        // Checking the Database size
        int databaseSizeBeforeCreation = authorRepository.findAll().size();

        // The request must fail and send an exception
        try {
            AuthorDTO failingAuthorDto = authorService.save(authorMapper.toDto(author));
            fail("Should throw exception when saving a new author without a name");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("An author must have a name"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }

    @DisplayName("Save Non Existing Author without Surname Test")
    @Test
    @Transactional
    public void testSaveNonExistingAuthorWithoutSurname() {
        // Creation of the author without Name
        author = createEntity();
        author.setSurname(null);

        // Checking the Database size
        int databaseSizeBeforeCreation = authorRepository.findAll().size();

        // The request must fail and send an exception
        try {
            AuthorDTO failingAuthorDto = authorService.save(authorMapper.toDto(author));
            fail("Should throw exception when saving a new author without a surname");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("An author must have a surname"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }



    /*
     ********UPDATE
     * ********SHOULD WORK
     */
    @DisplayName("Update an author Test")
    @Test
    @Transactional
    public void testUpdateAnAuthor() throws Exception {
        // Saving the author in the Database
        author = createEntity();
        AuthorDTO savedAuthorDto = authorService.save(authorMapper.toDto(author));

        int databaseSizeBeforeUpdate= authorRepository.findAll().size();

        // Updating the author
        Author author2 = createUpdatedEntity();
        author2.setId(savedAuthorDto.getId());
        AuthorDTO savedAuthor2Dto = authorService.save(authorMapper.toDto(author2));

        assertNotNull(savedAuthor2Dto);
        assertEquals(savedAuthorDto.getId(), savedAuthor2Dto.getId());
        assertEquals(UPDATED_NAME, savedAuthor2Dto.getName());
        assertEquals(UPDATED_SURNAME, savedAuthor2Dto.getSurname());

        // Checking that the size of the Database is still the same
        int databaseSizeAfterUpdate = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeUpdate, databaseSizeAfterUpdate);
    }


    /*
     ********UPDATE
     * ********SHOULD NOT WORK
     */
    @DisplayName("Update the Id of an author Test")
    @Test
    @Transactional
    public void testUpdateIdExistingAuthor() throws Exception {
        // Creation of the author
        author = createEntity();
        author.setId(1);

        // Saving the author in the Database
        AuthorDTO savedAuthorDto = authorService.save(authorMapper.toDto(author));

        // Setting another ID
        author.setId((2));

        // Checking the Database size
        int databaseSizeBeforeCreation = authorRepository.findAll().size();

        // The request must fail and send an exception
        try {
            authorService.save(authorMapper.toDto(author));
            fail("Should throw exception when saving a new author with an ID");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("The id of an author cannot be updated"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }

    @DisplayName("Update an Author without Id Test")
    @Test
    @Transactional
    public void testUpdateAuthorWithoutId() throws Exception {
        // Saving the author in the Database
        author = createEntity();
        AuthorDTO savedAuthorDto = authorService.save(authorMapper.toDto(author));

        // Checking the Database size
        int databaseSizeBeforeCreation = authorRepository.findAll().size();

        // The request must fail and send an exception
        try {
            authorService.save(authorMapper.toDto(author));
            fail("Should throw exception when saving an existing author without an Id");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("The author you are trying to save already exists in the Database"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }

    @DisplayName("Update an Author without Name Test")
    @Test
    @Transactional
    public void testUpdateAuthorWithoutName() throws Exception {
        // Saving the author in the Database
        author = createEntity();
        AuthorDTO savedAuthorDto = authorService.save(authorMapper.toDto(author));

        // Checking the Database size
        int databaseSizeBeforeCreation = authorRepository.findAll().size();

        // Setting a null name
        author.setName(null);
        author.setId(savedAuthorDto.getId());

        // The request must fail and send an exception
        try {
            AuthorDTO failingAuthorDto = authorService.save(authorMapper.toDto(author));
            fail("Should throw exception when updating an author without a name");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("An author must have a name"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }

    @DisplayName("Update an Author without Surname Test")
    @Test
    @Transactional
    public void testUpdateAuthorWithoutSurname() throws Exception {
        // Saving the author in the Database
        author = createEntity();
        AuthorDTO savedAuthorDto = authorService.save(authorMapper.toDto(author));

        // Checking the Database size
        int databaseSizeBeforeCreation = authorRepository.findAll().size();

        // Setting a null name
        author.setSurname(null);
        author.setId(savedAuthorDto.getId());

        // The request must fail and send an exception
        try {
            AuthorDTO failingAuthorDto = authorService.save(authorMapper.toDto(author));
            fail("Should throw exception when updating an author without a surname");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("An author must have a surname"));
        }

        // Checking that the size of the Database is still the same
        int databaseSizeAfterCreation = authorRepository.findAll().size();
        assertEquals(databaseSizeBeforeCreation, databaseSizeAfterCreation);
    }


    /*
     ********FIND ALL
     * ********SHOULD WORK
     */
    @DisplayName("Find all authors Test")
    @Test
    @Transactional
    public void testFindAllAuthors() throws Exception {
        author = createEntity();
        AuthorDTO savedAuthorDto = authorService.save(authorMapper.toDto(author));
        author2 = createUpdatedEntity();
        AuthorDTO savedAuthorDto2 = authorService.save(authorMapper.toDto(author2));


        int databaseSizeRep = authorRepository.findAll().size();
        int databaseSizeService = authorService.findAll().size();

        assertEquals(databaseSizeRep, databaseSizeService);
        assertNotNull(databaseSizeRep);
        assertNotNull(databaseSizeService);
    }

    /*
    ********FIND ALL
    * ********SHOULD NOT WORK
    */
    @DisplayName("Find all authors - no author in Database Test")
    @Test
    @Transactional
    public void testFindAllAuthorsNoAuthorInDB() {
        // Checking that the Database is empty
        ArrayList<Author> authorList = (ArrayList<Author>) authorRepository.findAll();
        assertTrue(authorList.isEmpty());

        // The request must fail and send an exception
        try {
            ArrayList<AuthorDTO> failingAuthorList = authorService.findAll();
            fail("Should throw exception when there is no author in the Database");
        } catch (Exception aExp) {
            assert (aExp.getMessage().contains("There is no author in the Database"));
        }
    }

    /*
     ********FIND ONE
     * ********SHOULD WORK
     */
    @DisplayName("Find an author with Id Test")
    @Test
    @Transactional
    public void testFindOneAuthors() throws Exception {
        // Saving the author in the Database
        author = createEntity();
        AuthorDTO savedAuthorDTO = authorService.save(authorMapper.toDto(author));

        Optional<Author> foundAuthorRepo = authorRepository.findById(savedAuthorDTO.getId());
        Optional<AuthorDTO> foundAuthorService = authorService.findOne(savedAuthorDTO.getId());

        assertTrue(foundAuthorRepo.isPresent());
        assertTrue(foundAuthorService.isPresent());
        assertEquals(foundAuthorRepo.get().getId(), foundAuthorService.get().getId());
        assertEquals(foundAuthorRepo.get().getName(), foundAuthorService.get().getName());
        assertEquals(foundAuthorRepo.get().getSurname(), foundAuthorService.get().getSurname());

    }
}
