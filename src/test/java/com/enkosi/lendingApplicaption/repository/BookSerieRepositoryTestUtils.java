package com.enkosi.lendingApplicaption.repository;

import com.enkosi.lendingApplicaption.domain.Author;
import com.enkosi.lendingApplicaption.domain.BookSerie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookSerieRepositoryTestUtils extends JpaRepository<BookSerie, Integer>  {

    void deleteAllByNameIsLike(String type);
}
