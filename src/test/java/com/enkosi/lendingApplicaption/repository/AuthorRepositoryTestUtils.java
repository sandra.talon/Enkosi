package com.enkosi.lendingApplicaption.repository;

import com.enkosi.lendingApplicaption.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepositoryTestUtils  extends JpaRepository<Author, Integer>  {

    void deleteAllByNameIsLike(String type);
}
