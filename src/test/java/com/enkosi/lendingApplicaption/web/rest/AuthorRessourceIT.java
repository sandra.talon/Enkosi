package com.enkosi.lendingApplicaption.web.rest;

import com.enkosi.lendingApplicaption.domain.Author;
import com.enkosi.lendingApplicaption.repository.AuthorRepository;
import com.enkosi.lendingApplicaption.repository.AuthorRepositoryTestUtils;
import com.enkosi.lendingApplicaption.service.AuthorService;
import com.enkosi.lendingApplicaption.service.dto.AuthorDTO;
import com.enkosi.lendingApplicaption.service.mapper.AuthorMapper;
import com.enkosi.lendingApplicaption.web_rest.AuthorRessource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Integration tests for {@Link AuthorRessource} REST controller.
 */
@SpringBootTest()
public class AuthorRessourceIT {

    private MockMvc restAuthorMockMvc;

    @Autowired
    private AuthorMapper authorMapper;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorRepositoryTestUtils authorRepositoryTestUtils;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;


    private Author author;
    private Author author2;

    private static String DEFAULT_NAME = "JULES";
    private static String DEFAULT_SURNAME = "DUPONT";

    private static String UPDATED_NAME = "MARC";
    private static String UPDATED_SURNAME = "DULAC";




    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthorRessource authorRessource = new AuthorRessource(authorService);
        this.restAuthorMockMvc = MockMvcBuilders.standaloneSetup(authorRessource)
                .setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Author createEntity(EntityManager em) {
        Author author = new Author();
        author.setName(DEFAULT_NAME);
        author.setSurname(DEFAULT_SURNAME);

        return author;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Author createUpdatedEntity(EntityManager em) {
        Author author = new Author();
        author.setName(UPDATED_NAME);
        author.setSurname(UPDATED_SURNAME);
        return author;
    }

    @BeforeEach
    public void initTest() {
        author = createEntity(em);
    }

    @AfterEach
    @BeforeEach
    public void cleanUpBetweenTest() {
        authorRepositoryTestUtils.deleteAllByNameIsLike(DEFAULT_NAME);
        authorRepositoryTestUtils.deleteAllByNameIsLike(UPDATED_NAME);

        authorRepositoryTestUtils.flush();
    }


    @DisplayName("create author Test")
    @Test
    @Transactional
    public void createAuthor() throws Exception {
        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create the author
        AuthorDTO authorDTO = authorMapper.toDto(author);
        restAuthorMockMvc.perform(post("/api/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(authorDTO)))
                .andExpect(status().isCreated());

        // Validate the author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate + 1);
        Author testAuthor = authorList.get(authorList.size() - 1);
        assertThat(testAuthor.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAuthor.getSurname()).isEqualTo(DEFAULT_SURNAME);
    }

    @DisplayName("create author with existing Id Test")
    @Test
    @Transactional
    public void createAuthorWithExistingId() throws Exception {
        // Save an author
        author = authorRepository.save(author);
        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create the Author with an existing Id
        AuthorDTO authorDTO = authorMapper.toDto(author);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorMockMvc.perform(post("/api/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(authorDTO)))
                .andExpect(status().isBadRequest());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate);
    }

    @DisplayName("Create Non Existing Author without Name Test")
    @Test
    @Transactional
    public void createAuthorWithoutName() {

        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create an new author without name
        author.setName(null);
        AuthorDTO authorDTO = authorMapper.toDto(author);

        // An author without a name cannot be created, so this API call must fail
        try
        {
            restAuthorMockMvc.perform(post("/api/authors")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(testUtils.convertObjectToJsonBytes(authorDTO)));
        }
        catch (Exception aExp) {
            assert (aExp.getMessage().contains("An author must have a name"));
        }

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate);
    }

    @DisplayName("Create Non Existing Author without Surname Test")
    @Test
    @Transactional
    public void createAuthorWithoutSurname() {

        int databaseSizeBeforeCreate = authorRepository.findAll().size();

        // Create an new author without name
        author.setSurname(null);
        AuthorDTO authorDTO = authorMapper.toDto(author);

        // An author without a surname cannot be created, so this API call must fail
        try
        {
            restAuthorMockMvc.perform(post("/api/authors")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(testUtils.convertObjectToJsonBytes(authorDTO)));
        }
        catch (Exception aExp) {
            assert (aExp.getMessage().contains("An author must have a surname"));
        }

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeCreate);
    }



    @Test
    @Transactional
    public void updateAuthor() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        int databaseSizeBeforeUpdate = authorRepository.findAll().size();

        // Update the author
        author2 = authorRepository.findById(author.getId()).get();
        // Disconnect from session so that the updates on updatedAuthor are not directly saved in db
        em.detach(author2);
        author2.setName(UPDATED_NAME);
        author2.setSurname(UPDATED_SURNAME);

        AuthorDTO authorDTO = authorMapper.toDto(author2);

        restAuthorMockMvc.perform(put("/api/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(authorDTO)))
                .andExpect(status().isOk());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeUpdate);
        Author testAuthor = authorList.get(authorList.size() - 1);
        assertThat(testAuthor.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAuthor.getSurname()).isEqualTo(UPDATED_SURNAME);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthor() throws Exception {
        int databaseSizeBeforeUpdate = authorRepository.findAll().size();

        // Create the Author
        AuthorDTO authorDTO = authorMapper.toDto(author);

        // If the author doesn't have an ID, it will throw BadRequestAlertException
        restAuthorMockMvc.perform(put("/api/authors")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(authorDTO)))
                .andExpect(status().isBadRequest());

        // Validate the Author in the database
        List<Author> authorList = authorRepository.findAll();
        assertThat(authorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void getAllAuthors() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);
        authorRepository.saveAndFlush(createUpdatedEntity(em));

        int databaseSizeBeforeUpdate = authorRepository.findAll().size();

        AuthorDTO authorDTO = authorMapper.toDto(author);

        // Get the authors
        restAuthorMockMvc.perform(get("/api/authors"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(author.getId())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(author.getName())))
                .andExpect(jsonPath("$.[*].surname").value(hasItem(author.getSurname())));

    }

    @Test
    @Transactional
    public void getAuthor() throws Exception {
        // Initialize the database
        authorRepository.saveAndFlush(author);

        // Get the author
        restAuthorMockMvc.perform(get("/api/authors/{id}", author.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(author.getId()))
                .andExpect(jsonPath("$.name").value(author.getName()))
                .andExpect(jsonPath("$.surname").value(author.getSurname()));
    }

}
