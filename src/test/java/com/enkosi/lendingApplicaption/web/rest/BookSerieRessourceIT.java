package com.enkosi.lendingApplicaption.web.rest;


import com.enkosi.lendingApplicaption.domain.BookSerie;
import com.enkosi.lendingApplicaption.repository.BookSerieRepository;
import com.enkosi.lendingApplicaption.repository.BookSerieRepositoryTestUtils;
import com.enkosi.lendingApplicaption.service.BookSerieService;
import com.enkosi.lendingApplicaption.service.dto.BookSerieDTO;
import com.enkosi.lendingApplicaption.service.mapper.BookSerieMapper;
import com.enkosi.lendingApplicaption.web_rest.BookSerieRessource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Integration tests for {@link BookSerieRessourceIT} REST controller.
 */
@SpringBootTest()
public class BookSerieRessourceIT {

    // TODO : populate database

    private MockMvc restBookSerieMockMvc;

    @Autowired
    private BookSerieMapper bookSerieMapper;

    @Autowired
    private BookSerieRepository bookSerieRepository;

    @Autowired
    private BookSerieRepositoryTestUtils bookSerieRepositoryTestUtils;

    @Autowired
    private BookSerieService bookSerieService;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;


    private BookSerie bookSerie;
    private BookSerie bookSerie2;

    private static String DEFAULT_NAME = "AAAAA";
    private static String UPDATED_NAME = "BBBBB";



    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BookSerieRessource bookSerieRessource = new BookSerieRessource(bookSerieService);
        this.restBookSerieMockMvc = MockMvcBuilders.standaloneSetup(bookSerieRessource)
                .setMessageConverters(jacksonMessageConverter)
                .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookSerie createEntity(EntityManager em) {
        BookSerie bookSerie = new BookSerie();
        bookSerie.setName(DEFAULT_NAME);
        return bookSerie;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookSerie createUpdatedEntity(EntityManager em) {
        BookSerie bookSerie = new BookSerie();
        bookSerie.setName(UPDATED_NAME);
        return bookSerie;
    }

    @BeforeEach
    public void initTest() {
        bookSerie = createEntity(em);
    }

    @AfterEach
    @BeforeEach
    public void cleanUpBetweenTest() {
        bookSerieRepositoryTestUtils.deleteAllByNameIsLike(DEFAULT_NAME);
        bookSerieRepositoryTestUtils.deleteAllByNameIsLike(UPDATED_NAME);

        bookSerieRepositoryTestUtils.flush();
    }


    @DisplayName("create bookSerie Test")
    @Test
    @Transactional
    public void createBookSerie() throws Exception {
        int databaseSizeBeforeCreate = bookSerieRepository.findAll().size();

        // Create the bookSerie
        BookSerieDTO bookSerieDTO = bookSerieMapper.toDto(bookSerie);
        restBookSerieMockMvc.perform(post("/api/bookSeries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(bookSerieDTO)))
                .andExpect(status().isCreated());

        // Validate the bookSerie in the database
        List<BookSerie> bookSerieList = bookSerieRepository.findAll();
        assertThat(bookSerieList).hasSize(databaseSizeBeforeCreate + 1);
        BookSerie testBookSerie = bookSerieList.get(bookSerieList.size() - 1);
        assertThat(testBookSerie.getName()).isEqualTo(DEFAULT_NAME);
    }

    @DisplayName("create bookSerie with existing Id Test")
    @Test
    @Transactional
    public void createBookSerieWithExistingId() throws Exception {
        // Save an bookSerie
        bookSerie = bookSerieRepository.save(bookSerie);
        int databaseSizeBeforeCreate = bookSerieRepository.findAll().size();

        // Create the BookSerie with an existing Id
        BookSerieDTO bookSerieDTO = bookSerieMapper.toDto(bookSerie);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookSerieMockMvc.perform(post("/api/bookSeries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(bookSerieDTO)))
                .andExpect(status().isBadRequest());

        // Validate the BookSerie in the database
        List<BookSerie> bookSerieList = bookSerieRepository.findAll();
        assertThat(bookSerieList).hasSize(databaseSizeBeforeCreate);
    }

    @DisplayName("Create Non Existing BookSerie without Name Test")
    @Test
    @Transactional
    public void createBookSerieWithoutName() {

        int databaseSizeBeforeCreate = bookSerieRepository.findAll().size();

        // Create an new bookSerie without name
        bookSerie.setName(null);
        BookSerieDTO bookSerieDTO = bookSerieMapper.toDto(bookSerie);

        // An bookSerie without a name cannot be created, so this API call must fail
        try
        {
            restBookSerieMockMvc.perform(post("/api/bookSeries")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(testUtils.convertObjectToJsonBytes(bookSerieDTO)));
        }
        catch (Exception aExp) {
            assert (aExp.getMessage().contains("An bookSerie must have a name"));
        }

        // Validate the BookSerie in the database
        List<BookSerie> bookSerieList = bookSerieRepository.findAll();
        assertThat(bookSerieList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void updateBookSerie() throws Exception {
        // Initialize the database
        bookSerieRepository.saveAndFlush(bookSerie);

        int databaseSizeBeforeUpdate = bookSerieRepository.findAll().size();

        // Update the bookSerie
        bookSerie2 = bookSerieRepository.findById(bookSerie.getId()).get();
        // Disconnect from session so that the updates on updatedBookSerie are not directly saved in db
        em.detach(bookSerie2);
        bookSerie2.setName(UPDATED_NAME);

        BookSerieDTO bookSerieDTO = bookSerieMapper.toDto(bookSerie2);

        restBookSerieMockMvc.perform(put("/api/bookSeries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(bookSerieDTO)))
                .andExpect(status().isOk());

        // Validate the BookSerie in the database
        List<BookSerie> bookSerieList = bookSerieRepository.findAll();
        assertThat(bookSerieList).hasSize(databaseSizeBeforeUpdate);
        BookSerie testBookSerie = bookSerieList.get(bookSerieList.size() - 1);
        assertThat(testBookSerie.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingBookSerie() throws Exception {
        int databaseSizeBeforeUpdate = bookSerieRepository.findAll().size();

        // Create the BookSerie
        BookSerieDTO bookSerieDTO = bookSerieMapper.toDto(bookSerie);

        // If the bookSerie doesn't have an ID, it will throw BadRequestAlertException
        restBookSerieMockMvc.perform(put("/api/bookSeries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testUtils.convertObjectToJsonBytes(bookSerieDTO)))
                .andExpect(status().isBadRequest());

        // Validate the BookSerie in the database
        List<BookSerie> bookSerieList = bookSerieRepository.findAll();
        assertThat(bookSerieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void getAllBookSeries() throws Exception {
        // Initialize the database
        bookSerieRepository.saveAndFlush(bookSerie);
        bookSerieRepository.saveAndFlush(createUpdatedEntity(em));

        int databaseSizeBeforeUpdate = bookSerieRepository.findAll().size();

        BookSerieDTO bookSerieDTO = bookSerieMapper.toDto(bookSerie);

        // Get the bookSeries
        restBookSerieMockMvc.perform(get("/api/bookSeries"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(bookSerie.getId())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(bookSerie.getName())));
    }

    @Test
    @Transactional
    public void getBookSerie() throws Exception {
        // Initialize the database
        bookSerieRepository.saveAndFlush(bookSerie);

        // Get the bookSerie
        restBookSerieMockMvc.perform(get("/api/bookSeries/{id}", bookSerie.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(bookSerie.getId()))
                .andExpect(jsonPath("$.name").value(bookSerie.getName()));
    }

}
